/*************
 * Arabesque *
 *************/
export type Middleware<Context> = (context: Context, next: () => Promise<void>) => Promise<void>;

export type Application<Channel, Context> = {
    use: (...middleware: Array<Middleware<Context>>) => void;
    listen: (channel: Channel) => Promise<() => Promise<void>>;
};

export type Listener<Channel, Context> = {
    start: (channel: Channel, handler: (context: Context) => Promise<void>) => Promise<() => Promise<void>>;
};

export const createApplication = <Channel, Context>(
    listener: Listener<Channel, Context>
): Application<Channel, Context> => {
    const middlewares: Array<Middleware<Context>> = [];

    const invokeMiddlewares = (context: Context, middlewares: Array<Middleware<Context>>): Promise<void> => {
        const middleware = middlewares[0];

        if (!middleware) {
            return Promise.resolve();
        }

        return middleware(context, () => {
            return invokeMiddlewares(context, middlewares.slice(1));
        });
    };

    return {
        use: (...middleware) => {
            middlewares.push(...middleware);
        },
        listen: (channel) => {
            return listener.start(channel, (context) => {
                return invokeMiddlewares(context, middlewares);
            });
        }
    };
};