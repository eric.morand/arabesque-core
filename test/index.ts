import * as tape from "tape";
import {createApplication} from "../src";

tape('Arabesque', ({test}) => {
    test('createApplication', ({end}) => {
        const application = createApplication<string, string>({
            start(channel, handler) {
                return handler(channel).then(() => {
                    return () => Promise.resolve();
                });
            }
        });

        application.use((context) => {
           console.log(`I am a middleware handling ${context}`);

           return Promise.resolve();
        });

        return application.listen('foo').then((stop) => {
            return stop();
        }).finally(() => {
            end();
        })
    });
});