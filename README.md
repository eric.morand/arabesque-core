# Arabesque

> Micro-services, in their simplest form

Arabesque is a lightweight, modular and functional framework for building micro-services.

## Sample

```ts
import {createApplication} from "@arabesque/core";

const application = createApplication<string, string>({
    start(channel, handler) {
        return handler(channel).then(() => {
            return () => Promise.resolve();
        });
    }
});

application.use((context) => {
    console.log(`I am a middleware handling ${context}`);

    return Promise.resolve();
});

return application.listen('foo').then((stop) => {
    return stop();
})
```
